<?php

/**
 * Page callback for menu item.
 */
function registration_url_actions_page_callback($registration, $action, $variable = '') {
  $wrapper = entity_metadata_wrapper('registration', $registration);
  $host = $wrapper->entity->value();
  $label = entity_label($registration->entity_type, $host);
  switch ($action) {
    case 'state':
      return drupal_get_form('registration_url_actions_state_form', $registration, $variable);
      break;
    case 'edit':
      return drupal_get_form('registration_form', $registration);
      break;
    case 'delete':
      return drupal_get_form('registration_url_actions_delete_form', $registration);
      break;
    default:
      drupal_not_found();
      break;
  }
}

/**
 * Confirm or edit state form callback.
 */
function registration_url_actions_state_form($form, &$form_state, $registration, $state = '') {

  $wrapper = entity_metadata_wrapper('registration', $registration);
  $host = $wrapper->entity->value();
  $path = entity_uri($registration->entity_type, $host);

  $states = array();
  if(empty($state)) {
    $states = registration_get_states_options(array('show_on_form' => TRUE, 'active' => TRUE));
    if (count($states) < 1) {
      drupal_set_message(t('The registration for %email is not changed. No valid states found.',
        array(
          '%email' => $wrapper->mail->value(),
        )
      ), 'error');
      drupal_goto($path['path']);
    }
    elseif (count($states) == 1) {
      $states = array_keys($states);
      $state = reset($states);
      $states = array();
    }
  }

  if (!empty($state)) {
    $old_state = $state;
    $state = registration_states(array('name' => $state, 'active' => TRUE));
    $state = reset($state);
    if (empty($state)) {
      drupal_set_message(t('The registration for %email is not changed. State %state is not valid.',
        array(
          '%email' => $wrapper->mail->value(),
          '%state' => $old_state,
        )
      ), 'error');
      drupal_goto($path['path']);
    }
  }

  $form['registration'] = array(
    '#type' => 'value',
    '#value' => $registration,
  );

  if (!empty($state)) {
    if ($state->name == $wrapper->state->value()->name) {
      drupal_set_message(t('The registration for %email was already set to %state.',
        array(
          '%email' => $wrapper->mail->value(),
          '%state' => $state->label,
        )
      ));
      drupal_goto($path['path']);
    }
    $form['state'] = array(
      '#type' => 'value',
      '#value' => $state->name,
    );
    return confirm_form($form,
      t('Are you sure you want to change the registration on %title for %email to %state?',
        array(
          '%title' => entity_label($registration->entity_type, $host),
          '%email' => $wrapper->mail->value(),
          '%state' => $state->label,
        )
      ),
      $path['path'],
      t('Please confirm this change.'),
      t('Confirm'),
      t('Cancel')
    );
  }

  $form['state'] = array(
    '#type' => 'select',
    '#title' => t('State'),
    '#description' => t('State of this registration'),
    '#default_value' => $wrapper->state->value()->name,
    '#options' => $states,
  );

  $form['actions'] = array('#type' => 'actions');
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Change'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'link',
    '#title' => t('Cancel'),
    '#href' => $path['path'],
  );

  return $form;
}

/**
 * Submit callback for state change confirmation.
 */
function registration_url_actions_state_form_submit($form, &$form_state) {
  $registration = $form_state['values']['registration'];
  $wrapper = entity_metadata_wrapper('registration', $registration);
  $host = $wrapper->entity->value();
  $path = entity_uri($registration->entity_type, $host);

  $state = registration_states(array('name' => $form_state['values']['state'], 'active' => TRUE));
  $state = reset($state);

  $registration->state = $state->name;
  registration_save($registration);

  drupal_set_message(t('Registration on %title for %email has been set to %state.',
    array(
      '%title' => entity_label($registration->entity_type, $host),
      '%email' => $wrapper->mail->value(),
      '%state' => $state->label,
    )
  ));

  $form_state['redirect'] = $path['path'];

}

/**
 * Confirm form callback for deletion.
 */
function registration_url_actions_delete_form($form, &$form_state, $registration) {
  $wrapper = entity_metadata_wrapper('registration', $registration);
  $host = $wrapper->entity->value();
  $path = entity_uri($registration->entity_type, $host);

  $form['registration'] = array(
    '#type' => 'value',
    '#value' => $registration,
  );

  return confirm_form($form,
    t('Are you sure you want to delete registration on %title for %email?',
      array(
        '%title' => entity_label($registration->entity_type, $host),
        '%email' => $wrapper->mail->value(),
      )
    ),
    $path['path'],
    t('This action cannot be undone.'),
    t('Delete'),
    t('Cancel')
  );
}

/**
 * Submit callback for delete confirmation.
 */
function registration_url_actions_delete_form_submit($form, &$form_state) {
  if ($form_state['values']['confirm']) {
    $registration = $form_state['values']['registration'];
    $wrapper = entity_metadata_wrapper('registration', $registration);
    $host = $wrapper->entity->value();
    $path = entity_uri($registration->entity_type, $host);
    registration_delete_multiple(array($registration->registration_id));

    watchdog('registration', 'Registration %id deleted.', array('%id' => $registration->registration_id));
    drupal_set_message(t('Registration on %title for %email has been deleted.',
      array(
        '%title' => entity_label($registration->entity_type, $host),
        '%email' => $wrapper->mail->value(),
      )
    ));

    $form_state['redirect'] = $path['path'];
  }
}